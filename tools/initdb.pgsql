-- Drops matches table
DROP TABLE IF EXISTS matches;

-- Creates matches table
CREATE TABLE IF NOT EXISTS matches (
    id INT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY
    , user_id varchar(50) NOT NULL
    , game varchar(50) NOT NULL
    , time bigint NULL 
    , players INT[] NULL
    , date date NOT NULL
    , matchtime time NOT NULL
);