import axios from "axios";
import * as M from "materialize-css";
import Vue from "vue";

// tslint:disable-next-line no-unused-expression
new Vue({
  computed: {
    hazMatches(): boolean {
      return this.isLoading === false && this.matches.length > 0;
    },
    noMatches(): boolean {
      return this.isLoading === false && this.matches.length === 0;
    },
  },
  data() {
    return {
      game: "",
      time: "",
      matches: [],
      isLoading: true,
      players: [],
      selectedMatch: "",
      selectedMatchId: 0,
      date: "",
      matchtime: "",
    };
  },
  el: "#app",
  methods: {
    addMatch() {
      console.log(this.date);
      console.log(this.matchtime);
      const match = {
        game: this.game,
        date: this.date,
        matchtime: this.matchtime,
      };
      axios
        .post("/api/matches/add", match)
        .then(() => {
          this.game = "";
          this.time = 0;
          this.date = "";
          this.matchtime = "";
          this.loadMatches();
        })
        .catch((err: any) => {
          // tslint:disable-next-line:no-console
          console.log(err);
        });
    },
    formatDate(date: string) {
      const formattedDate = new Date(date).toLocaleDateString('de-DE');
      return formattedDate;
    },
    formatTime(time: string) {
      return time.substr(0,5);
    }
    confirmDeleteMatch(id: string) {
      const match = this.matches.find((g) => g.id === id);
      this.selectedMatch = `${match.game} ${match.time}`;
      this.selectedMatchId = match.id;
      const dc = this.$refs.deleteConfirm;
      const modal = M.Modal.init(dc);
      modal.open();
    },
    deleteMatch(id: strReferenceError: addMatch is not defineding) {
      axios
        .delete(`/api/matches/remove/${id}`)
        .then(this.loadMatches)
        .catch((err: any) => {
          // tslint:disable-next-line:no-console
          console.log(err);
        });
    },
    loadMatches() {
      axios
        .get("/api/matches/all")
        .then((res: any) => {
          this.isLoading = false;
          this.matches = res.data;
        })
        .catch((err: any) => {
          // tslint:disable-next-line:no-console
          console.log(err);
        });
    },
  },
  mounted() {
    return this.loadMatches();
  },
});
