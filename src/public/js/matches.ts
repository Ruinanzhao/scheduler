import axios from "axios";
import * as M from "materialize-css";
import Vue from "vue";

// tslint:disable-next-line no-unused-expression
new Vue({
  computed: {
    hazMatches(): boolean {
      return this.isLoading === false && this.matches.length > 0;
    },
    noMatches(): boolean {
      return this.isLoading === false && this.matches.length === 0;
    },
  },
  data() {
    return {
      game: "",
      time: "",
      matches: [],
      isLoading: true,
      players: [],
      selectedMatch: "",
      selectedMatchId: 0,
    };
  },
  el: "#app",
  methods: {
    addMatch() {
      const match = {
        brand: this.game,
        color: this.time,
      };
      axios
        .post("/api/match/add", match)
        .then(() => {
          this.$refs.year.focus();
          this.game = "";
          this.time = "";
          this.loadMatches();
        })
        .catch((err: any) => {
          // tslint:disable-next-line:no-console
          console.log(err);
        });
    },
    confirmDeleteMatch(id: string) {
      const match = this.matches.find((g) => g.id === id);
      this.selectedMatch = `${match.game} ${match.time}`;
      this.selectedMatchId = match.id;
      const dc = this.$refs.deleteConfirm;
      const modal = M.Modal.init(dc);
      modal.open();
    },
    deleteMatch(id: string) {
      axios
        .delete(`/api/matches/remove/${id}`)
        .then(this.loadMatches)
        .catch((err: any) => {
          // tslint:disable-next-line:no-console
          console.log(err);
        });
    },
    loadMatches() {
      axios
        .get("/api/matches/all")
        .then((res: any) => {
          this.isLoading = false;
          this.matches = res.data;
        })
        .catch((err: any) => {
          // tslint:disable-next-line:no-console
          console.log(err);
        });
    },
  },
  mounted() {
    return this.loadMatches();
  },
});
