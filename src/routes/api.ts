import * as express from "express";
import pgPromise from "pg-promise";

export const register = (app: express.Application) => {
  const oidc = app.locals.oidc;
  const port = parseInt(process.env.PGPORT || "5432", 10);
  const config = {
    database: process.env.PGDATABASE || "postgres",
    host: process.env.PGHOST || "localhost",
    port,
    user: process.env.PGUSER || "postgres",
  };

  const pgp = pgPromise();
  const db = pgp(config);

  app.get(
    `/api/matches/all`,
    oidc.ensureAuthenticated(),
    async (req: any, res) => {
      try {
        const userId = req.userContext.userinfo.sub;
        const matches = await db.any(
          `
                SELECT
                    id
                    , game
                    , date
                    , matchtime
                FROM    matches
                ORDER BY date, matchtime`,
          { userId }
        );
        return res.json(matches);
      } catch (err) {
        // tslint:disable-next-line:no-console
        console.error(err);
        res.json({ error: err.message || err });
      }
    }
  );

  app.get(
    `/api/matches/total`,
    oidc.ensureAuthenticated(),
    async (req: any, res) => {
      try {
        const userId = req.userContext.userinfo.sub;
        const total = await db.one(
          `
            SELECT  count(*) AS total
            FROM    matches`,
          { userId },
          (data: { total: number }) => {
            return {
              total: +data.total,
            };
          }
        );
        return res.json(total);
      } catch (err) {
        // tslint:disable-next-line:no-console
        console.error(err);
        res.json({ error: err.message || err });
      }
    }
  );

  app.get(
    `/api/matches/find/:search`,
    oidc.ensureAuthenticated(),
    async (req: any, res) => {
      try {
        const userId = req.userContext.userinfo.sub;
        const matches = await db.any(
          `
                SELECT
                    id
                    , game
                    , time
                FROM    matches
                WHERE   ( game ILIKE $[search] OR time ILIKE $[search] )`,
          { userId, search: `%${req.params.search}%` }
        );
        return res.json(matches);
      } catch (err) {
        // tslint:disable-next-line:no-console
        console.error(err);
        res.json({ error: err.message || err });
      }
    }
  );

  app.post(
    `/api/matches/test`,
    oidc.ensureAuthenticated(),
    async (req: any, res) => {
      try {
        const userId = req.userContext.userinfo.sub;
        const id = await db.one(
          `
                INSERT INTO matches( user_id, game, time )
                VALUES( $[userId], $[game], $[time] )
                RETURNING id;`,
          { userId, ...req.body }
        );
        return res.json({ id });
      } catch (err) {
        // tslint:disable-next-line:no-console
        console.error(err);
        res.json({ error: err.message || err });
      }
    }
  );

  app.post(
    `/api/matches/add`,
    oidc.ensureAuthenticated(),
    async (req: any, res) => {
      try {
        const userId = req.userContext.userinfo.sub;
        const id = await db.one(
          `
                INSERT INTO matches( user_id, game, date, matchtime )
                VALUES( $[userId], $[game], $[date], $[matchtime] )
                RETURNING id;`,
          { userId, ...req.body }
        );
        return res.json({ id });
      } catch (err) {
        // tslint:disable-next-line:no-console
        console.error(err);
        res.json({ error: err.message || err });
      }
    }
  );

  app.post(
    `/api/matches/update`,
    oidc.ensureAuthenticated(),
    async (req: any, res) => {
      try {
        const userId = req.userContext.userinfo.sub;
        const id = await db.one(
          `
                UPDATE matches
                SET players = '{1,2,3}'
                WHERE
                    id = $[id]
                RETURNING
                    id;`,
          { userId, ...req.body }
        );
        return res.json({ id });
      } catch (err) {
        // tslint:disable-next-line:no-console
        console.error(err);
        res.json({ error: err.message || err });
      }
    }
  );

  app.delete(
    `/api/matches/remove/:id`,
    oidc.ensureAuthenticated(),
    async (req: any, res) => {
      try {
        const userId = req.userContext.userinfo.sub;
        const id = await db.result(
          `
                DELETE
                FROM    matches
                WHERE     id = $[id]`,
          { userId, id: req.params.id },
          (r) => r.rowCount
        );
        return res.json({ id });
      } catch (err) {
        // tslint:disable-next-line:no-console
        console.error(err);
        res.json({ error: err.message || err });
      }
    }
  );
};
