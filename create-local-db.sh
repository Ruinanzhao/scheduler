#! /bin/sh

docker pull postgres:latest
docker run -d --name matches-db -p 5432:5432 -e 'POSTGRES_PASSWORD=p@ssw0rd42' postgres
