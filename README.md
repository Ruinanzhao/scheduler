A simple webapplication that allows users to schedule stuff with other people

To run:

1. npm install

2. If you have docker just run the create-local-db.sh script

3. Create a .env file by copying the .env.dist

4. npm start
